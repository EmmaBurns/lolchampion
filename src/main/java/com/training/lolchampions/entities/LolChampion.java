package com.training.lolchampions.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="lolchampion")
public class LolChampion {

    @Id
    @GeneratedValue
    @Column(name="ChampionID")
    private Long id;
    @Column(name="name")
    private String name;
    @Column(name="role")
    private String role;
    @Column(name="difficulty")
    private String Difficulty;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDifficulty() {
        return Difficulty;
    }

    public void setDifficulty(String difficulty) {
        Difficulty = difficulty;
    }
}
