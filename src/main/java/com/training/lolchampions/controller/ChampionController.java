package com.training.lolchampions.controller;

import com.training.lolchampions.entities.LolChampion;
import com.training.lolchampions.service.ChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class ChampionController {

    @Autowired
    private ChampionService ChampionService;

    @GetMapping
    public List<LolChampion> findAll(){
        return ChampionService.findAll();
    }
    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion){
        return ChampionService.save(lolChampion);
    }
    @DeleteMapping("/remove")
    public void delete(@RequestBody LolChampion lolChampion){
        ChampionService.delete(lolChampion);
    }

    @DeleteMapping("/removebyid/{id}")
    public void deleteById(@PathVariable(value = "id") Long id){
        ChampionService.deleteById(id);
    }
}
