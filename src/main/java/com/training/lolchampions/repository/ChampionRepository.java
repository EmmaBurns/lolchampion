package com.training.lolchampions.repository;

import com.training.lolchampions.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ChampionRepository extends JpaRepository<LolChampion, Long> {

}