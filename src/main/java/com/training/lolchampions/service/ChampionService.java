package com.training.lolchampions.service;

import com.training.lolchampions.repository.ChampionRepository;
import com.training.lolchampions.entities.LolChampion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChampionService {


    @Autowired
    ChampionRepository championRepository;

    public List<LolChampion> findAll() {
        return championRepository.findAll();
    }

    public LolChampion save(LolChampion lolChampion) {
        return championRepository.save(lolChampion);
    }

    public void delete(LolChampion lolChampion) {
        championRepository.delete(lolChampion);
    }

    public void deleteById(Long id) {
        championRepository.deleteById(id);
    }
}

